//app.js 
var https = require('utils/https.js')
App({
  globalData: {
    domain: "https://www.zinglizingli.xyz",
    
    token:null,
    userInfo: null
  },
   onLaunch: function () {
    
    
  },
  trySetUserInfo: function () {
    var userInfo = wx.getStorageSync('userInfo');
    if (userInfo) {
      this.globalData.userInfo = userInfo;
    }
  },
  removeCache:function(){
    var value = wx.getStorageSync('cateListTime')
    var nowTime = new Date().getTime();
    if(value){
      if (value < nowTime) {
        wx.removeStorage("cateList");
      }
    }
    value = wx.getStorageSync('searchKeysTime')
    if (value) {
      if (value < nowTime) {
        wx.removeStorage("searchKeysTime");
      }
    }
  }
})